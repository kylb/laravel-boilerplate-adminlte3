<div class="card-body">
    <div class="form-group row">

        {!! Form::label('email', 'E-mail', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-3 ">
            {!! Form::text('email',null,['class' => 'form-control input-jqv', 'id' => 'email', 'autocomplete' =>
            'username' ]) !!}
        </div>
        @error('name')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror
        {!! Form::label('password', 'Senha', ['class' => 'col-sm-1 control-label']) !!}
        <div class=" col-sm-3">
            {!! Form::password('password',['class' => 'form-control input-jqv', 'id' => 'password', 'autocomplete' =>
            'new-password', 'data-toggle' => 'password']) !!}
            <div class="input-group-append">
                <span class="input-group-text input-password-hide" style="cursor: pointer;"><i
                        class="fa fa-eye"></i></span>
            </div>
            @error('password')
            <span class="invalid-feedback d-block"> {{ $message }} </span>
            @enderror
        </div>

        {!! Form::label('name', 'Nome Completo', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-3">
            {!! Form::text('name',null,['class' => 'form-control input-jqv', 'id' => 'name']) !!}
        </div>
        @error('name')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror
        
    </div>

    {{-- <div class="form-group row">
        {!! Form::label('tax', 'CPF', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-3">
            {!! Form::text('tax',null,['class' => 'form-control input-jqv cpf', 'id' => 'tax']) !!}
        </div>
        @error('tax')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror
        {!! Form::label('birth', 'Dt. Nasc.', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-3">
            {!! Form::text('birth',null,['class' => 'form-control input-jqv placeholder datepicker', 'id' => 'birth',
            // 'data-provide' => "datepicker",
            ]) !!}
        </div>
        @error('birth')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror
    </div> --}}

    {{-- <div class="form-group row">

        {!! Form::label('phone_area', 'DDD', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-1">
            {!! Form::text('phone_area',null,['class' => 'form-control input-jqv', 'id' => 'phone_area']) !!}
        </div>
        @error('phone_area')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror

        {!! Form::label('phone_number', 'Telefone', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-2">
            {!! Form::text('phone_number',null,['class' => 'form-control input-jqv', 'id' => 'phone_number']) !!}
        </div>
        @error('phone_number')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror
        {!! Form::label('address_zip', 'CEP', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-2">
            {!! Form::text('address_zip',null,['class' => 'form-control input-jqv cep', 'id' => 'cep',
            // 'data-provide' => "datepicker",
            ]) !!}
        </div>
        @error('address_zip')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror
        {!! Form::label('address_city', 'Cidade', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-2">
            {!! Form::text('address_city',null,['class' => 'form-control input-jqv', 'id' => 'cidade']) !!}
        </div>
        @error('address_city')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror
        <div class="col-sm-1">
            {!! Form::text('address_state',null,['class' => 'form-control input-jqv', 'id' => 'uf',
            'placeholder' => 'UF']) !!}
        </div>
        @error('address_state')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror
    </div> --}}

    {{-- <div class="form-group row">

        {!! Form::label('address_street', 'Rua', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-4">
            {!! Form::text('address_street',null,['class' => 'form-control input-jqv', 'id' => 'rua']) !!}
        </div>
        @error('address_street')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror

        {!! Form::label('address_district', 'Bairro', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-4">
            {!! Form::text('address_district',null,['class' => 'form-control input-jqv', 'id' => 'bairro']) !!}
        </div>
        @error('address_district')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror

        {!! Form::label('address_number', 'Num.', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-1">
            {!! Form::text('address_number',null,['class' => 'form-control input-jqv', 'id' => 'address_number', 'placeholder' =>
            'Núm.']) !!}
        </div>
        @error('address_number')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror

    </div> --}}

    {{-- <div class="form-group row">
        {!! Form::label('address_complement', 'Complemento', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-4">
            {!! Form::text('address_complement',null,['class' => 'form-control input-jqv', 'id' => 'address_complement']) !!}
        </div>

        @error('address_complement')
        <span class="invalid-feedback d-block"> {{ $message }} </span>
        @enderror

    </div> --}}

</div>

<div class="card-footer">
    <div class="box-footer">
        <a class="btn btn-danger float-right"
            href="{{ (auth()->user()->can('index', [App\User::class, auth()->user()])) ? route('panel.user.index') : route('panel.index') }}">Voltar</a> {!!
        Form::submit('Salvar',['class'
        => 'btn btn-primary pull-left']) !!}
    </div>
</div>
@section('css')
<link rel="stylesheet" href="/panel/js/plugins/jquery-ui/jquery-ui.min.css">
@endsection
@section('js_form')
<script type="text/javascript" src="/panel/js/via-cep.js"></script>
<script type="text/javascript" src="/panel/js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script>
    $(document).ready(function() {
            // $("#form_id").validate();
            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                nextText: 'Próximo',
                prevText: 'Anterior'
            });
        });
</script>
@stop